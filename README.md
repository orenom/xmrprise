# xmrprise

[xmrpri.se](https://xmrpri.se)

Simple website for following the price of Monero.

* No JavaScript or ads
* Lightweight, no unnecessary stuff
* Self-hostable

Uses the [CoinGecko API](https://www.coingecko.com/en/api) under the hood.

XMR: 832ogRwuoSs2JGYg7wJTqshidK7dErgNdfpenQ9dzMghNXQTJRby1xGbqC3gW3GAifRM9E84J91VdMZRjoSJ32nkAZnaCEj

# Self-hosting, installation

1. Install [Node.js](https://nodejs.org).

1. Clone and set up the repository.

   ```console
   git clone https://codeberg.org/orenom/xmrprise
   cd xmrprise
   npm install --no-optional
   cp src/config.js.template src/config.js # edit the config file to suit your environment
   npm start
   ```
xmrprise should now be running.
