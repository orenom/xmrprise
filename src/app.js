const http = require('http')
const https = require('https')
const url = require('url')
const fs = require('fs')
const qs = require('querystring')
const tmpl = require('blueimp-tmpl')
const asciichart = require('asciichart')
const config = require('./config')

/**
* Many, if not all of these currencies might be "valid" in the context of this
* software, but in real life these currencies enables surveillance!
* Use Monero, and stay safe.
*/
const valid_currencies = {  
  // s - symbol
  // p - symbol place (0 before the price, 1 after the price)
  // d - number of digits after the decimal point
  'eur': { s: '€', p: 1, d: 2 },
  'usd': { s: '$', p: 0, d: 2 },
  'btc': { s: '฿', p: 0, d: 6 },
  'eth': { s: 'Ξ', p: 0, d: 2 },
  'ltc': { s: 'Ł', p: 0, d: 2 },
  'bch': { s: '', p: 0, d: 2 },
  'bnb': { s: '', p: 0, d: 2 },
  'eos': { s: '', p: 0, d: 2 },
  'xrp': { s: '', p: 0, d: 2 },
  'xlm': { s: '', p: 0, d: 2 },
  'link': { s: '', p: 0, d: 2 },
  'dot': { s: '', p: 0, d: 2 },
  'yfi': { s: '', p: 0, d: 2 },
  'aed': { s: '', p: 0, d: 2 },
  'ars': { s: '', p: 0, d: 2 },
  'aud': { s: '', p: 0, d: 2 },
  'bdt': { s: '', p: 0, d: 2 },
  'bhd': { s: '', p: 0, d: 2 },
  'brl': { s: '', p: 0, d: 2 },
  'bmd': { s: '', p: 0, d: 2 },
  'cad': { s: '', p: 0, d: 2 },
  'chf': { s: '', p: 0, d: 2 },
  'clp': { s: '', p: 0, d: 2 },
  'cny': { s: '', p: 0, d: 2 },
  'czk': { s: '', p: 0, d: 2 },
  'dkk': { s: '', p: 0, d: 2 },
  'gbp': { s: '', p: 0, d: 2 },
  'hkd': { s: '', p: 0, d: 2 },
  'huf': { s: '', p: 0, d: 2 },
  'idr': { s: 'Rp', p: 0, d: 2 },
  'ils': { s: '', p: 0, d: 2 },
  'inr': { s: '', p: 0, d: 2 },
  'jpy': { s: '', p: 0, d: 2 },
  'krw': { s: '', p: 0, d: 2 },
  'kwd': { s: '', p: 0, d: 2 },
  'lkr': { s: '', p: 0, d: 2 },
  'mmk': { s: '', p: 0, d: 2 },
  'mxn': { s: '', p: 0, d: 2 },
  'myr': { s: '', p: 0, d: 2 },
  'ngn': { s: '', p: 0, d: 2 },
  'nok': { s: '', p: 0, d: 2 },
  'nzd': { s: '', p: 0, d: 2 },
  'php': { s: '', p: 0, d: 2 },
  'pkr': { s: '', p: 0, d: 2 },
  'pln': { s: '', p: 0, d: 2 },
  'rub': { s: '', p: 0, d: 2 },
  'sar': { s: '', p: 0, d: 2 },
  'sek': { s: '', p: 0, d: 2 },
  'sgd': { s: '', p: 0, d: 2 },
  'thb': { s: '', p: 0, d: 2 },
  'try': { s: '', p: 0, d: 2 },
  'twd': { s: '', p: 0, d: 2 },
  'uah': { s: '', p: 0, d: 2 },
  'vef': { s: '', p: 0, d: 2 },
  'vnd': { s: '', p: 0, d: 2 },
  'zar': { s: '', p: 0, d: 2 },
  'xdr': { s: '', p: 0, d: 2 },
  'xag': { s: '', p: 0, d: 2 },
  'xau': { s: '', p: 0, d: 2 },
  'bits': { s: '', p: 0, d: 2 },
  'sats': { s: '', p: 0, d: 2 },
}

/**
* Cache the results from the CoinGecko API per currency. For both price and
* market data.
*/
const currency_cache_price = []
const currency_cache_market = []

// Valid durations for our ASCII chart
const valid_durations_chart = ['1d', '7d', '30d', '90d', '1y', 'all']

if(config.USE_HTTPS) {
  const privateKey = fs.readFileSync(`${config.CERT_DIR}/privkey.pem`, 'utf8')
  const certificate = fs.readFileSync(`${config.CERT_DIR}/cert.pem`, 'utf8')
  const ca = fs.readFileSync(`${config.CERT_DIR}/chain.pem`, 'utf8')
  const credentials = {
	  key: privateKey,
	  cert: certificate,
	  ca: ca
  }
  server(https, config.SSL_PORT, credentials)
}

server(http, config.PORT)

function server(app, port, options={}) {
  app.createServer(options, async (req, res) => {
    if(config.USE_HTTPS) {
      if(Object.keys(options).length === 0) {
        // let's redirect HTTP to HTTPS
        res.writeHead(302, { 'Location': 'https://' + req.headers.host + req.url })
        return res.end()
      }
    }
    
    const valid_urls = ['/', '/preferences', '/getPrice', '/styles.css']
    
    if(!valid_urls.includes(req.url)) {
      res.writeHead(404, { 'Content-Type': 'text/plain' })
      return res.end('404 not found')
    }
    
    let queries = url.parse(req.url, true).query
    
    if(req.method === 'POST') {
      if(req.url === '/preferences') {
        let body = ''

        req.on('data', (data) => {
          body += data

          // too much POST data, kill the connection
          // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
          if(body.length > 1e6) {
            req.connection.destroy()
          }
        })

        req.on('end', () => {
          let post = qs.parse(body)
          let currency = 'eur'
          let hide_symbol = false
          let price_size = 5
          let auto_update = 0
          let update_with_js = false
          
          if(post['currency']) {
            currency = post['currency']
          }
          
          if(post['hide_symbol'] === 'on') {
            hide_symbol = true
          }
          
          if(post['price_size']) {
            price_size = parseInt(post['price_size'])
          }
          
          if(post['auto_update']) {
            auto_update = parseInt(post['auto_update'])
          }
          
          if(post['update_with_js'] === 'on') {
            update_with_js = true
            auto_update = 0
          }
          
          let expires = new Date(new Date().getTime()+31536000*1000).toUTCString() // 1 year
          let cookie_flags = `SameSite=Strict; Secure; expires=${expires};`
          res.writeHead(302, {
            'Location': '/',
            'Set-Cookie': [`currency=${currency}; ${cookie_flags}`,
                           `hide_symbol=${hide_symbol}; ${cookie_flags}`,
                           `price_size=${price_size}; ${cookie_flags}`,
                           `auto_update=${auto_update}; ${cookie_flags}`,
                           `update_with_js=${update_with_js}; ${cookie_flags}`
                          ],
          })
          res.end()
        })
      }
      
      if(req.url === '/getPrice') {
        let body = ''

        req.on('data', (data) => {
          body += data

          // Too much POST data, kill the connection!
          // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
          if(body.length > 1e6) {
            req.connection.destroy()
          }
        })
        
        req.on('end', async () => {
          let currency = config.DEFAULT_CURRENCY
          let cookies = parseCookies(req)
          let symbol
          
          if(cookies.currency) {
            currency = cookies.currency
          }
          
          if(cookies.hide_symbol == 'true') {
            symbol = ''
          } else {
            symbol = valid_currencies[currency].s
          }
          
          const price = await getPrice(currency)
          
          res.writeHead(200, { 'Content-Type': 'application/json' })
          res.end(JSON.stringify({ price: price, symbol: symbol, decimals: valid_currencies[currency].d }))
        })
      }
    }
    
    if(req.method === 'GET') {
      if(req.url === '/' || !req.url) {
        let currency = config.DEFAULT_CURRENCY
        let cookies = parseCookies(req)
        let price_size = 5
        let auto_update = 0
        let update_with_js = false
        let symbol
        
        if(cookies.currency) {
          currency = cookies.currency
        }
        
        if(!(currency in valid_currencies)) {
          currency = config.DEFAULT_CURRENCY
        }
        
        if(cookies.hide_symbol == 'true') {
          symbol = ''
        } else {
          symbol = valid_currencies[currency].s
        }
        
        if(cookies.price_size) {
          price_size = parseInt(cookies.price_size)
        }
        
        if(cookies.auto_update) {
          auto_update = parseInt(cookies.auto_update)
          if(auto_update < 5 && auto_update !== 0) {
            res.writeHead(500, { 'Content-Type': 'text/plain' })
            res.end('maybe not so fast okay')
          }
        }
        
        if(cookies.update_with_js == 'true') {
          update_with_js = true
          auto_update = 0
        }
        
        if(!queries.d) {
          queries.d = '1d'
        }
        
        const price = await getPrice(currency)
        const chart = await getChart(currency, queries.d)
        
        data = {
          price: parseFloat(price).toFixed(valid_currencies[currency].d),
          chart: chart,
          currency: currency,
          symbol: symbol,
          symbol_place: valid_currencies[currency].p,
          chart_duration: queries.d,
          valid_durations_chart: valid_durations_chart,
          valid_currencies: valid_currencies,
          price_size: price_size,
          auto_update: auto_update,
          update_with_js: update_with_js
        }
        
        let expires = new Date(new Date().getTime()+31536000*1000).toUTCString() // 1 year
        let cookie_flags = `SameSite=Strict; Secure; expires=${expires};`
        res.writeHead(200, {
          'Content-Type': 'text/html',
          'Set-Cookie': [`currency=${currency}; ${cookie_flags}`,
                         `hide_symbol=${cookies.hide_symbol}; ${cookie_flags}`,
                         `price_size=${price_size}; ${cookie_flags}`,
                         `auto_update=${auto_update}; ${cookie_flags}`,
                         `update_with_js=${update_with_js}; ${cookie_flags}`
                        ],
        })
        
        tmpl.load = (id) => {
          return fs.readFileSync(`./${id}.html`, 'utf8')
        }
        res.end(tmpl('index', data))
      } else {
        res.writeHead(200, { 'Content-Type': 'text/css' })
        res.end(fs.readFileSync('.' + req.url, 'utf8'), 'utf-8')
      }
    }
    
  }).listen(port)
}

function parseCookies(req) {
  let list = {}
  let rc = req.headers.cookie

  rc && rc.split(';').forEach((cookie) => {
      let parts = cookie.split('=')
      list[parts.shift().trim()] = decodeURI(parts.join('='))
  })

  return list
}

async function getPrice(currency) {
  return new Promise((resolve) => {
    currency = currency.toLowerCase()
    let diff
    if(currency_cache_price[currency]) {
      diff = Date.now() - currency_cache_price[currency].fetched
    }
    if(diff <= config.CACHE_TIME_PRICE) {
      resolve(currency_cache_price[currency].price)
    } else {
      https.get(`https://api.coingecko.com/api/v3/simple/price?ids=monero&vs_currencies=${currency}`, (res) => {
        console.log('Fetched data from CoinGecko API')
        if(res.statusCode !== 200) {
          console.log('Error while fetching price')
          resolve(`Error while getting price data, HTTP status code: ${res.statusCode}`)
        }
        
        let data = ''

        res.on('data', (chunk) => {
          data += chunk
        })

        res.on('end', () => {
          try {
            data = JSON.parse(data)
            resolve(data['monero'][currency])
            if(currency_cache_price[currency] == undefined) {
              currency_cache_price[currency] = {}
            }
            currency_cache_price[currency] = {
              fetched: Date.now(),
              price: data['monero'][currency]
            }
          } catch(err) {
            console.log('Error while parsing fetched price', err)
            resolve(`Error while parsing fetched price`)
          }
        })

      }).on('error', (err) => {
        console.log('Error while fetching price', err)
      })
    }
  })
}

async function getChart(currency, duration) {
  return new Promise((resolve) => {
    currency = currency.toLowerCase()
    if(!(currency in valid_currencies)) {
      resolve('invalid currency')
    }
    let diff
    if(currency_cache_market[currency]) {
      if(currency_cache_market[currency][duration]) {
        diff = Date.now() - currency_cache_market[currency][duration].fetched
        console.log(diff)
      }
    }
    if(diff <= config.CACHE_TIME_CHART[duration]) {
      resolve(currency_cache_market[currency][duration].chart)
    } else {
      let now = Math.round(Date.now() / 1000)
      let to = now
      let from = now - (24 * 3600)
      
      if(duration === '1d') {
        from = now - (24 * 3600)
      }
      if(duration === '7d') {
        from = now - (24 * 3600 * 7)
      }
      if(duration === '30d') {
        from = now - (24 * 3600 * 30)
      }
      if(duration === '90d') {
        from = now - (24 * 3600 * 90)
      }
      if(duration === '1y') {
        from = now - (24 * 3600 * 365)
      }
      if(duration === 'all') {
        from = 1230760800
      }
      
      https.get(`https://api.coingecko.com/api/v3/coins/monero/market_chart/range?vs_currency=${currency}&from=${from}&to=${to}`, (res) => {
        console.log('Fetched market data from CoinGecko API')
        if(res.statusCode !== 200) {
          console.log('Error while fetching market data')
          resolve(`Error while getting the market data. HTTP status code from CoinGecko API: ${res.statusCode} ${res.statusMessage}`)
        }
        
        let data = ''

        res.on('data', (chunk) => {
          data += chunk
        })

        res.on('end', () => {
          try {
            data = JSON.parse(data)
            
            // remove the first element from each array, we don't want the time data
            for(let i = 0; i < data.prices.length; i++) {
              data.prices[i] = data.prices[i].pop()
            }
           
            // let's reduce the length of the array to 120, so it doesn't grow in width too much
            let numIndicesInRange = data.prices.length / 120
            let prev = 0
            let cur = numIndicesInRange
            let reduced = []

            for(let i = 0; i < 120; i++) {
              let num = 0
              let sum = 0
              for(let j = Math.ceil(prev); j < cur && j < data.prices.length - 1; j++) {  
                num++
                sum += data.prices[j]
              }
              
              prev += numIndicesInRange
              cur += numIndicesInRange
              if(sum !== 0 && num !== 0) {
                reduced[i] = sum / num
              }
            }
            
            // pass the data to asciichart()
            let ascii_chart = asciichart.plot(reduced, { height: 40 })
            resolve(ascii_chart)
            
            // save the ascii chart to our cache
            if(currency_cache_market[currency] == undefined) {
              currency_cache_market[currency] = {}
            }
            if(currency_cache_market[currency][duration] == undefined) {
              currency_cache_market[currency][duration] = {}
            }
            currency_cache_market[currency][duration] = {
              fetched: Date.now(),
              chart: ascii_chart
            }
          } catch(err) {
            console.log('Error while parsing fetched market data', err)
            resolve(`Error while parsing fetched market data. ${JSON.stringify(err)}`)
          }
        })

      }).on('error', (err) => {
        console.log('Error while fetching market data', err)
        resolve(`Error while getting the market data. ${JSON.stringify(err)}`)
      })
    }
  })
}
